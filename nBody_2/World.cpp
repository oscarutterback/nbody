//
//  World.cpp
//  nBody_2
//
//  Created by Oscar Utterbäck on 23/01/15.
//  Copyright (c) 2015 jaidzadza. All rights reserved.
//

#include "World.h"
#include <iostream>
#include <math.h>
#include <random>
#include <iterator>
#include <set>
#include <algorithm>

typedef std::pair<std::vector<Body>::iterator, std::vector<Body>::iterator> it_pair;

using std::cout;
using std::endl;

World::World(int n_bodies, double time_step, double grav_const): _n_bodies(n_bodies), _time_step(time_step), _gravity_const(grav_const) {
    cout << "Creating world" << endl;
    cout << "Number of bodies: " << n_bodies << endl;
    _dist_limited = true;
    _vel_limited = false;
    _min_distance = 0.01;
    _max_velocity = 1e10;
    
    _aggregate_bodies = false;
    
    _force_sign = 1;
    _force_exponent = -1.0;
    Body::_cnt = 0;
    
}

void World::addBody(Body b) {
    _bodies.push_back(b);
    
}


void World::updateBodyPos() {

    for (auto b = std::begin(_bodies); b != std::end(_bodies); ++b) {
        if (!b->_fixed) {
            b->_pos += _time_step*b->_vel;
        }
    }
    
}

void World::updateBodyVel() {
    auto b_end = std::end(_bodies);

    std::vector<it_pair> to_erase;
    // cout << "updateBodyVel()" << endl;
    for (auto b_i = std::begin(_bodies); b_i != b_end; ++b_i) {
        for (auto b_j = std::next(b_i); b_j != b_end; ++b_j) {
            Eigen::Vector2d r_ij = (b_j->_pos - b_i->_pos);
            double distance = r_ij.norm();

            if (distance < std::max(b_i->_mass, b_j->_mass)*2) {
                if (_dist_limited && distance < _min_distance) {
                    distance = _min_distance;
                }
                
                r_ij.normalize();
                
                if (_aggregate_bodies && distance < 10) {
                    to_erase.push_back({b_i, b_j});
                    
                } else {
                    Eigen::Vector2d commonFactor = (_time_step * _gravity_const ) * r_ij * pow(distance, _force_exponent);
                    if (!b_i->_fixed) {
                        b_i->_vel += _force_sign * commonFactor * b_j->_mass;
                    }
                    if (!b_j->_fixed) {
                        b_j->_vel -= _force_sign * commonFactor * b_i->_mass;
                    }
                }
                
                
            }
            
        }

    }
    
    if (_aggregate_bodies && !to_erase.empty()) {
        
        std::set<int> right;
        
        for (auto e = to_erase.rbegin(); e != to_erase.rend(); ++e) {
            //cout << "Attempting to merge:";
            //cout << e->first->_id << " <- " << e->second->_id << endl;
            double m_1 = e->first->_mass;
            double m_2 = e->second->_mass;
            right.insert(e->second->_id);

            //cout << "Updating velocity and mass" << endl;
            Eigen::Vector2d new_vel = (m_1 * e->first->_vel + m_2 * e->second->_vel) / (m_1 + m_2);
            e->first->_mass += e->second->_mass;
            e->first->_vel = new_vel;
            
            
        }
        
        _bodies.erase(std::remove_if(std::begin(_bodies), std::end(_bodies), [&](Body b) {return right.find(b._id) != right.end(); }));
        
        to_erase.clear();
    }
    
}

void World::clearWorld() {
    Body::_cnt = 0;
    _bodies.clear();
}

int World::livingBodies() {
    return _bodies.size();
}

void World::limitVelocities() {
    for (auto b = std::begin(_bodies); b != std::end(_bodies);) {
        if (b->_vel.norm() > _max_velocity) {
            b->_vel.normalize();
            b->_vel *= _max_velocity;
        }
        ++b;
    }
    
}

std::vector<Body> World::getBodies() const {
    return _bodies;
}

void updateParameter(World::world_param, double) {
    
}

void World::print() {
    for (auto b: _bodies) {
        printf("body(id: %4d) at p: (%f, %f), v: (%f, %f) \n", b._id, b._pos.x(), b._pos.y(), b._vel.x(), b._vel.y());
    }
}
