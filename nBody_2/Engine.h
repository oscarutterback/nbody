//
//  Engine.h
//  nBody_2
//
//  Created by Oscar Utterbäck on 23/01/15.
//  Copyright (c) 2015 jaidzadza. All rights reserved.
//

#ifndef __nBody_2__Engine__
#define __nBody_2__Engine__

#include <iostream>
#include "World.h"
#include "config.h"



class Engine {
public:
    Engine(World);
    void drawWorld();
    void generateWorld();
    World _world;
    
private:
    
};

#endif /* defined(__nBody_2__Engine__) */
