//
// Disclamer:
// ----------
//
// This code will work only if you selected window, graphics and audio.
//
// Note that the "Run Script" build phase will copy the required frameworks
// or dylibs to your application bundle so you can execute it on any OS X
// computer.
//
// Your resource files (images, sounds, fonts, ...) are also copied to your
// application bundle. To get the path to these resource, use the helper
// method resourcePath() from ResourcePath.hpp
//


#define N_BODIES 600
#define GRAVITY_CONST 1.0
#define TIME_STEP 1e-2
#define FONT_SIZE 16

#define FIXED_SIZE 15
#define FREE_SIZE 10

#define TEXT_TOP_OFFSET 10
#define TEXT_RIGHT_OFFSET 10

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include <iostream>
#include <random>
#include <string>

#include <Eigen/Dense>

// Here is a small helper for you ! Have a look.
#include "ResourcePath.hpp"


#include "config.h"

#include "Body.h"
#include "World.h"
#include "Engine.h"
// #include "Integrate.h"


using std::cout;
using std::endl;
//using std::string
using Eigen::Vector2d;


sf::Font _font;

void generateWorld(World& _world) {
    _world.clearWorld();
    
    std::random_device rd;
    std::mt19937 gen(rd());
    
    std::uniform_int_distribution<> pos_x(-400, SCREEN_WIDTH - 200);
    std::uniform_int_distribution<> pos_y(-350, SCREEN_HEIGHT - 150);
    
    pos_x = std::uniform_int_distribution<>(-(SCREEN_WIDTH + 500), SCREEN_WIDTH + 500);
    pos_y = std::uniform_int_distribution<>(-(SCREEN_HEIGHT + 200), SCREEN_HEIGHT + 200);

    std::uniform_int_distribution<> vel(-1000, 1000);
    std::uniform_int_distribution<> mass_dist(-500, 500);
    
    for (int i = 0; i < _world._n_bodies; i++) {
        int p_x = pos_x(gen);
        int p_y = pos_y(gen);
        int mod = 0;
        int v_x = mod*(p_y - SCREEN_HEIGHT/2); // vel(gen);
        int v_y = -mod*(p_x - SCREEN_WIDTH/2); //  vel(gen);
        
        int r = (p_x) % 255;
        int g = (p_y) % 255;
        int bl = (p_x*p_y) % 255;
        
        sf::Color c(r, g, bl);
        
        double mass = mass_dist(gen);
        Eigen::Vector2d pos(p_x, p_y);
        Eigen::Vector2d vel(v_x, v_y);
        
        Body b(pos, vel, mass, c);
        _world.addBody(b);
    }
    pos_x = std::uniform_int_distribution<>(-(SCREEN_WIDTH + 500), SCREEN_WIDTH + 500);
    pos_y = std::uniform_int_distribution<>(-(SCREEN_HEIGHT + 200), SCREEN_HEIGHT + 200);
    for (int i = 0; i < 0; ++i) {
        int p_x = pos_x(gen);
        int p_y = pos_y(gen);
        int mod = 300;
        int v_x = mod*(p_y - SCREEN_HEIGHT/2); // vel(gen);
        int v_y = -mod*(p_x - SCREEN_WIDTH/2); //  vel(gen);
        double mass = 20000*mass_dist(gen);
        Eigen::Vector2d pos(p_x, p_y);
        Eigen::Vector2d vel(v_x, v_y);
        
        
        sf::Color c(200, 200, 200);
        Body b(pos, vel, mass, c);
        b._fixed = true;
        _world.addBody(b);

    }
}

sf::Text prepareText(std::string text, sf::Vector2f pos) {
    sf::Text sfText;
    sfText.setFont(_font);
    sfText.setString(text);
    sfText.setCharacterSize(FONT_SIZE);
    sfText.setColor(sf::Color::White);
    sfText.setPosition(pos);
    return sfText;
    
}

sf::Text textUpperRight(std::string text) {
    sf::Text t = prepareText(text, {0, 0});
    double w = t.getLocalBounds().width;
    t.setPosition(SCREEN_WIDTH - (w + TEXT_RIGHT_OFFSET), TEXT_TOP_OFFSET);
    return t;
}


sf::Text textLowerRight(std::string text) {
    sf::Text t = prepareText(text, {0, 0});
    double w = t.getLocalBounds().width;
    t.setPosition(SCREEN_WIDTH - (w + TEXT_RIGHT_OFFSET), SCREEN_HEIGHT - TEXT_TOP_OFFSET);
    return t;
}

struct Camera {
    sf::Vector2f pos, dim;
};


int main(int, char const**)
{
    
    cout << " - Starting nBody v0.2" << endl;
    cout << "   Enjoy !" << endl;
    // Create the main window
    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "nBody simulator 0.2");

    // Set the Icon
    sf::Image icon;
    if (!icon.loadFromFile(resourcePath() + "icon.png")) {
        return EXIT_FAILURE;
    }
    window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());

    // Load a sprite to display
    sf::Texture texture;
    if (!texture.loadFromFile(resourcePath() + "cute_image.jpg")) {
        return EXIT_FAILURE;
    }
    sf::Sprite sprite(texture);

    // Create a graphical text to display
    sf::Font font;
    if (!font.loadFromFile(resourcePath() + "sansation.ttf")) {
        return EXIT_FAILURE;
    }
    
    // Load a music to play
    sf::Music music;
    if (!music.openFromFile(resourcePath() + "bgm.wav")) {
        return EXIT_FAILURE;
    }
    music.setLoop(true);

    // Play the music
    music.play();
    if (!_font.loadFromFile(resourcePath() + "Minecraftia-Regular.ttf")) {
        cout << "Could not load font. Exiting" << endl;
        return EXIT_FAILURE;
    }

    World _world(N_BODIES, TIME_STEP, GRAVITY_CONST);
    generateWorld(_world);
    
    sf::View view;
    view.reset(sf::FloatRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
    window.setView(view);

    
    sf::Vector2i last_mouse_pos(0, 0);
    
    bool paused = false;
    bool slowDown = false;
    bool print = false;
    bool mouseDrag = false;
    double mouseDragMultiplier = 1.0;
    double zoomScale = 1.0;
    int selected = -1;
    std::ostringstream oss_status_text;
    
    sf::Clock clock;
    
    sf::Time status_text_time;
    
    // Start the game loop
    while (window.isOpen())
    {
        
        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Close window: exit
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            // Escape pressed: exit
            if (event.type == sf::Event::KeyPressed) {
                
                switch (event.key.code) {
                    case sf::Keyboard::Escape:
                        window.close();
                        break;
                    case sf::Keyboard::P:
                        paused = !paused;
                        break;
                        
                    case sf::Keyboard::R:
                        _world.clearWorld();
                        generateWorld(_world);
                        break;
                        
                    case sf::Keyboard::O:
                        slowDown = !slowDown;
                        break;
                        
                    case sf::Keyboard::I:
                        print = !print;
                        break;
                    case sf::Keyboard::J:
                        view.reset(sf::FloatRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
                        window.setView(view);
                        break;
                        
                    case sf::Keyboard::V:
                        {
                        sf::Vector2f size = view.getSize();
                        size.x *= 1.3;
                        size.y *= 1.3;
                        view.setSize(size);
                        window.setView(view);
                        mouseDragMultiplier *= 1.3;
                        }
                        break;
                    case sf::Keyboard::C:
                        {
                        sf::Vector2f size = view.getSize();
                        size.x /= 1.3;
                        size.y /= 1.3;
                        view.setSize(size);
                        window.setView(view);
                        mouseDragMultiplier /= 1.3;
                        }
                        break;
                        
                    case sf::Keyboard::G:
                        {sf::Vector2f cen = view.getCenter();
                        cout << "view center:" << cen.x << " " << cen.y << endl;}
                        break;
                        
                    case sf::Keyboard::Left:
                        _world._time_step *= 1.5;
                        oss_status_text << "Time step increased.\n" << "new value: " << _world._time_step << "\n";
                        clock.restart();
                        break;
                        
                    case sf::Keyboard::Right:
                        _world._time_step /= 1.5;
                        oss_status_text << "Time step decreased.\n" << "new value: " << _world._time_step << "\n";
                        clock.restart();
                        break;
                        
                        
                    case sf::Keyboard::Up:
                        _world._force_exponent += 0.1;
                        oss_status_text << "Force exponent increased.\n" << "new value: " << _world._force_exponent << "\n";
                        clock.restart();
                        break;
                    case sf::Keyboard::Down:
                        _world._force_exponent -= 0.1;
                        oss_status_text << "Force exponent decreased.\n" << "new value: " << _world._force_exponent << "\n";
                        clock.restart();
                        break;
                    
                    case sf::Keyboard::Q:
                        _world._force_sign *= -1;
                        oss_status_text << "Force sign switched: ";
                        if (_world._force_sign < 0) {
                            oss_status_text << "Repulsive" << "\n";
                        } else {
                            oss_status_text << "Attractive" << "\n";
                            
                        }
                        clock.restart();
                        break;
                        
                    case sf::Keyboard::BackSpace:
                        oss_status_text.str("");
                        oss_status_text.clear();
                        break;
                        
                    case sf::Keyboard::A:
                        _world._n_bodies += 100;
                        oss_status_text << "Number of bodies increased: " << _world._n_bodies << "\n";
                        clock.restart();
                        break;
                    case sf::Keyboard::Z:
                        _world._n_bodies -= 100;
                        oss_status_text << "Number of bodies decreased: " << _world._n_bodies << "\n";
                        clock.restart();
                        break;
                        
                    default:
                        break;
                }
            }
            
            if (event.type == sf::Event::MouseButtonReleased) {
                if (event.mouseButton.button == sf::Mouse::Right) {
                    mouseDrag = false;
                    //last_mouse_pos = {0, 0};
                }
            }
                
            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                sf::Vector2i pos = sf::Mouse::getPosition(window);
                
                for (auto b : _world.getBodies()) {
                    Eigen::Vector2d b_pos = b.getPos();
                    sf::Vector2f sf_mouse_pos = window.mapPixelToCoords(pos);
                    Eigen::Vector2d mouse_pos(sf_mouse_pos.x, sf_mouse_pos.y);
                    
                    double distance = (mouse_pos - b_pos).norm();
                    if (distance < 10.0 ) {
                        cout << "distance: " << distance;
                        selected = b._id;
                        break;
                    }
                }
                cout << "mouse: " << pos.x << " " << pos.y << endl;
            }
            
            if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
                if (!mouseDrag) {
                    mouseDrag = true;
                    last_mouse_pos = sf::Mouse::getPosition(window);
                }
                sf::Vector2i pos = sf::Mouse::getPosition(window);
                //cout << "pos: " << pos.x << " " << pos.y << endl;
                sf::Vector2f offset(-mouseDragMultiplier*(pos - last_mouse_pos).x, -mouseDragMultiplier*(pos-last_mouse_pos).y);
                //cout << "offset: " << offset.x << " " << offset.y << endl;
                view.move(offset);
                window.setView(view);
                
                last_mouse_pos = pos;
            }
            
            
        }

        // Clear screen
        window.clear();
        
        
        for (auto b : _world.getBodies()) {
            sf::CircleShape dot;
            if (b._fixed) {
                dot = sf::CircleShape(FIXED_SIZE);
            } else {
                dot = sf::CircleShape(FREE_SIZE);
            }
            


            Vector2d pos = b.getPos();
            dot.setPosition(pos.x(), pos.y());

            dot.setFillColor(b._color);
            
            
            if (b._id == selected) {
                int selection_box_size;
                if (b._fixed) {
                    selection_box_size = FIXED_SIZE*2;
                } else {
                    selection_box_size = FREE_SIZE*2;
                }
                sf::RectangleShape selectionBox(sf::Vector2f(selection_box_size, selection_box_size));
                selectionBox.setOutlineThickness(1);
                selectionBox.setFillColor(sf::Color::Transparent);
                selectionBox.setOutlineColor(sf::Color::White);
                selectionBox.setPosition(dot.getPosition().x, dot.getPosition().y);
                window.draw(selectionBox);
                
                
                window.setView(window.getDefaultView());
                sf::Vector2i infoBoxPos = {20, 20};
                sf::Vector2f truePos = window.mapPixelToCoords(infoBoxPos);

                std::ostringstream oss_pos;
                Vector2d vel = b.getVel();
                
                oss_pos << "body: " << b._id << "\n";
                
                oss_pos << "pos: (" << (int) pos.x() << ", " << (int) pos.y() << ")\n"
                    << "vel: (" << (int64_t) vel.x() << ", " << (int64_t)vel.y() << ")";
                
                oss_pos << "\nmass: " << b._mass;
                sf::Vector2f text_pos = {10.0, 15.0};
                text_pos += truePos;
                sf::Text pos_text = prepareText(oss_pos.str(), text_pos);
                
                
                sf::RectangleShape infoBox(sf::Vector2f(pos_text.getLocalBounds().width + 20, 100));
                infoBox.setPosition(truePos.x, truePos.y);
                infoBox.setOutlineThickness(1);
                infoBox.setFillColor(sf::Color(0, 0, 0, 120));
                infoBox.setOutlineColor(sf::Color(255, 255, 255, 200));
                window.draw(infoBox);
                window.draw(pos_text);

                window.setView(view);
            }
            window.draw(dot);
            
        }
        
        if (!paused) {
            _world.updateBodyPos();
            _world.updateBodyVel();
            if (_world._vel_limited) {
                _world.limitVelocities();
                
            }
        } else { // paused
            sf::RectangleShape rect(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
            rect.setFillColor(sf::Color(0, 0, 0, 100));
            window.draw(rect);
            sf::Text t = prepareText("Paused", sf::Vector2f(200, 200));
            
            window.draw(t);
        }
        if (print) {
            _world.print();
        }
        
        window.setView(window.getDefaultView());
        
        std::string n_text = "Number of bodies: " + std::to_string(_world.livingBodies());
        sf::Text sf_n_text = textLowerRight(n_text);
        window.draw(sf_n_text);
        
        std::string status_text = oss_status_text.str();
        
        if (!status_text.empty()) {
            sf::Text sf_status_text = textUpperRight(status_text);
            window.draw(sf_status_text);
            if (clock.getElapsedTime().asMilliseconds() >= 1000) {
                oss_status_text.str("");
                oss_status_text.clear();
            }
            
        }
        window.setView(view);

        
        window.display();
        
        if (slowDown) {
            sf::sleep(sf::milliseconds(500));
        }
    
    }

    return EXIT_SUCCESS;
}
