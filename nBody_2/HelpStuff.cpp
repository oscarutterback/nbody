//
//  HelpStuff.cpp
//  nBody_2
//
//  Created by Oscar Utterbäck on 26/01/15.
//  Copyright (c) 2015 jaidzadza. All rights reserved.
//

#include "HelpStuff.h"
#include <SFML/System/Vector2.hpp>
#include <math.h>

namespace HelpStuff {
    double norm(sf::Vector2f v) {
        return sqrt(v.x*v.x + v.y*v.y);
    }
}