//
//  config.h
//  nBody_2
//
//  Created by Oscar Utterbäck on 23/01/15.
//  Copyright (c) 2015 jaidzadza. All rights reserved.
//

#ifndef nBody_2_config_h
#define nBody_2_config_h

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

#endif
