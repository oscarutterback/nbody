//
//  World.h
//  nBody_2
//
//  Created by Oscar Utterbäck on 23/01/15.
//  Copyright (c) 2015 jaidzadza. All rights reserved.
//

#ifndef __nBody_2__World__
#define __nBody_2__World__

#include <iostream>
#include <vector>
#include "Body.h"




class World {
    
public:
    enum world_param {
        GRAVITY,
        TIMESTEP,
        NUM_BODIES
    };

    
    World(int, double, double);
    
    void addBody(Body);
    
    void updateBodyPos();
    void updateBodyVel();
    void updateParameter(world_param, double);
    
    void limitVelocities();
    std::vector<Body> getBodies() const;

    void print();
    
    void generateBodies(int);
    void combineBodies(Body, Body);
    void breakBody(Body);
    int livingBodies();

    void clearWorld();
   

    int _n_bodies;
    double _time_step;
    double _gravity_const;
    
    int _force_sign;
    double _force_exponent;
    
    double _max_velocity;
    double _min_distance;
    
    bool _vel_limited;
    bool _dist_limited;
    
    bool _aggregate_bodies;

private:
    std::vector<Body> _bodies;
    
    
};

#endif /* defined(__nBody_2__World__) */
