//
//  Body.h
//  nBody_2
//
//  Created by Oscar Utterbäck on 23/01/15.
//  Copyright (c) 2015 jaidzadza. All rights reserved.
//

#ifndef __nBody_2__Body__
#define __nBody_2__Body__

#include <iostream>
#include <SFML/Graphics/Color.hpp>
#include <Eigen/Dense>

class Body {

friend class World;
    
public:
    Body(Eigen::Vector2d, Eigen::Vector2d, double, sf::Color);

    Eigen::Vector2d getPos();
    Eigen::Vector2d getVel();
    
    void print();
    static int _cnt;
    int _id;
    bool _fixed;
    sf::Color _color;
    Eigen::Vector2d _pos, _vel;
    double _mass;
private:
    

};


#endif /* defined(__nBody_2__Body__) */
