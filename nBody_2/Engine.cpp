//
//  Engine.cpp
//  nBody_2
//
//  Created by Oscar Utterbäck on 23/01/15.
//  Copyright (c) 2015 jaidzadza. All rights reserved.
//


#include <random>
#include <Eigen/Dense>

Engine::Engine(World world) : _world(world) {
    
}

void Engine::drawWorld() {
    
}

void Engine::generateWorld() {
    _world.clearWorld();
    
    std::random_device rd;
    std::mt19937 gen(rd());
    
    std::uniform_int_distribution<> pos_x(200, SCREEN_WIDTH - 200);
    std::uniform_int_distribution<> pos_y(150, SCREEN_HEIGHT - 150);
    std::uniform_int_distribution<> vel(-1000, 1000);
    std::uniform_int_distribution<> mass_dist(2000, 6000);

    for (int i = 0; i < _world._n_bodies; i++) {
        int p_x = pos_x(gen);
        int p_y = pos_y(gen);
        int mod = 300;
        int v_x = mod*(p_y - SCREEN_HEIGHT/2); // vel(gen);
        int v_y = -mod*(p_x - SCREEN_WIDTH/2); //  vel(gen);
        double mass = 200*mass_dist(gen);
        Eigen::Vector2d pos(p_x, p_y);
        Eigen::Vector2d vel(v_x, v_y);
        
        Body b(pos, vel, mass);
        _world.addBody(b);
    }
    
}