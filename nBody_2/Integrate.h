//
//  Integrate.h
//  nBody_2
//
//  Created by Oscar Utterbäck on 23/01/15.
//  Copyright (c) 2015 jaidzadza. All rights reserved.
//

#ifndef __nBody_2__Integrate__
#define __nBody_2__Integrate__

#include <iostream>
#include <Eigen/Dense>

#endif /* defined(__nBody_2__Integrate__) */
namespace Integrate {
    void explicit_euler(Eigen::Vector2d, double);
}