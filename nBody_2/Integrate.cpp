//
//  Integrate.cpp
//  nBody_2
//
//  Created by Oscar Utterbäck on 23/01/15.
//  Copyright (c) 2015 jaidzadza. All rights reserved.
//

#include "Integrate.h"
void Integrate::explicit_euler(Eigen::Vector2d state, double time_step) {
    std::cout << "Explicit_Euler!" << std::endl;
    std::cout << state.x() << " " << state.y() << " " << time_step << std::endl;
    
}
