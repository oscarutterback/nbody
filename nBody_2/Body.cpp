//
//  Body.cpp
//  nBody_2
//
//  Created by Oscar Utterbäck on 23/01/15.
//  Copyright (c) 2015 jaidzadza. All rights reserved.
//

#include "Body.h"

int Body::_cnt;

Body::Body(Eigen::Vector2d pos, Eigen::Vector2d vel, double mass, sf::Color color) : _pos(pos), _vel(vel), _mass(mass), _color(color) {
    _id = _cnt++;
    _fixed = false;
    //printf("Created body(id: %d) at p: (%f, %f), v: (%f, %f) \n", _id, _pos.x(), _pos.y(), _vel.x(), _vel.y());

}

Eigen::Vector2d Body::getPos() {
    return _pos;
}
Eigen::Vector2d Body::getVel() {
    return _vel;
}

void Body::print() {
    printf("body(id: %d) at p: (%f, %f), v: (%f, %f) \n", _id, _pos.x(), _pos.y(), _vel.x(), _vel.y());
}
